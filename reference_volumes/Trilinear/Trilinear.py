#!/usr/bin/python

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License version 3 for more details.
#
#    You should have received a copy of the GNU General Public License version 3
#    along with this program (LICENSE.txt).  If not, see http://www.gnu.org/licenses/

import numpy as np
import math

N=128
# 128x128x128 Trilinear-cube

volume=np.zeros( (N,N,N), dtype=np.float32)

for i in range(0,N):
    for j in range(0,N):
        for k in range(0,N):
            volume[i,j,k] = (i+j+k)/float(3*N)
volume.tofile("trilinear_%ix%ix%i.dat" % (N,N,N))
