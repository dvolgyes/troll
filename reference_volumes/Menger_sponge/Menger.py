#!/usr/bin/python

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License version 3 for more details.
#
#    You should have received a copy of the GNU General Public License version 3
#    along with this program (LICENSE.txt).  If not, see http://www.gnu.org/licenses/


import numpy as np
import math

# 81x81x81 Menger sponge, https://en.wikipedia.org/wiki/Menger_sponge

levels=4
N=int(math.pow(3,levels))

volume=np.zeros( (N,N,N), dtype=np.float32)

def genstring(i,s=None):
    S=s if s!=None else np.ones( (1), np.float32)
    if i==0: return S
    n=S.size
    t=np.zeros( (3*n), dtype=np.float32 )
    t[0:n]=S*(3.0/2.0)
    t[n:2*n] = 1
    t[2*n:3*n]=S*(3.0/2.0)
    return genstring(i-1,t)
    
s = genstring(levels)

for i in range(0,N):
    for j in range(0,N):
        for k in range(0,N):
            volume[i,j,k] = s[i]*s[j]*s[k]
volume[volume==1]=0
volume.tofile("menger_%ix%ix%i.dat" % (N,N,N))
