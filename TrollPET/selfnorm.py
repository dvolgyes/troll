#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np
import scipy


class Sensitivity(object):

    def __init__(self):
        self.shape = dict()
        pass

    def setup(self, *args, **kwargs):
        pass

    def add_lors(
        self,
        calc,
        meas,
        bg,
        sens,
        m,
        ):

        self.shape[m] = calc.shape[1:]

    def calculate(self, *args, **kwargs):
        pass

    def get_sens(self, m):
        return np.ones(self.shape[m], dtype=np.float32)

    def save(self, *args, **kwargs):
        pass

    def set_png(self, name):
        self.png = name

    def set_output(self, name):
        self.output = name

    def set_limit(self, limit):
        self.limit = limit


class CrystalSensitivity2N(object):

    def __init__(self):
        pass

    def setup(
        self,
        N,
        c1,
        c2,
        coinc,
        ):

        self.N = N
        self.crystals = c1 * c2
        self.c1 = c1
        self.c2 = c2
        self.coinc = coinc
        self.mpairs = int(self.N / 2) * coinc
        self.pairs = dict()
        for i in range(0, int(N / 2)):
            self.pairs[i] = (i, i + int(N / 2))
        for j in range(1, coinc):
            for i in range(0, N):
                self.pairs[i + j * N - int(N / 2)] = (i, (i + int(N / 2) - j) % N)

        self.sens_c = dict()
        self.sens_m = dict()
        for i in range(0, self.mpairs):
            self.sens_m[i] = 1.0
        for i in range(0, self.N):
            self.sens_c[i] = np.ones(self.crystals, dtype=np.float32)

        self.cLORs = dict()
        self.mLORs = dict()
        self.sens = dict()
        self.bg = dict()
        self.M = dict()
        self.C = dict()

        for i in range(0, self.N):
            self.C[i] = np.zeros((self.crystals, ), dtype=np.float32)
            self.M[i] = np.zeros((self.crystals, ), dtype=np.float32)

        self.png = None
        self.output = None
        self.disable_mpairs = True

    def set_limit(self, limit):
        self.limit = limit

    def set_png(self, name):
        self.png = name

    def set_output(self, name):
        self.output = name

    def add_lors(
        self,
        calc,
        meas,
        bg,
        sens,
        m,
        ):

        self.cLORs[m] = np.sum(calc, axis=0).reshape(self.crystals, -1)
        self.mLORs[m] = np.sum(meas, axis=0).reshape(self.crystals, -1)
        self.sens[m] = np.sum(sens, axis=0).reshape(self.crystals, -1)
        if bg != None:
            self.bg[m] = np.sum(bg, axis=0)
        else:
            self.bg[m] = None

    def reset(self):
        for i in range(0, self.N):
            self.sens_c[i].fill(1)
        for i in range(0, self.mpairs):
            self.cLORs[i].fill(0)
            self.mLORs[i].fill(0)
            self.sens[i].fill(1)
            self.sens_m[i] = 1

    def calculate(self, iteration):
        for i in range(0, self.limit):
            for m in range(0, self.mpairs):
                cLORs2 = self.get_sens(m)
                cLORs2 *= self.cLORs[m]
                cLORs2 *= self.sens[m]
                if self.bg[m] != None:
                    cLORs2 += self.bg[m].reshape(cLORs2.shape)

                self.sens_m[m] *= np.sum(self.mLORs[m]) / np.sum(cLORs2)

            c = sum(self.sens_m.values()) / len(self.sens_m)

            for m in range(0, self.mpairs):
                self.sens_m[m] = self.sens_m[m] / c

            if self.disable_mpairs:
                for m in range(0, self.mpairs):
                    self.sens_m[m] = 1.0

            for m in range(0, self.mpairs):
                cLORs2 = self.get_sens(m) * self.cLORs[m]
                if self.bg[m] != None:
                    cLORs2 += self.bg[m].reshape(cLORs2.shape)

                C1 = np.sum(cLORs2, axis=1)
                C2 = np.sum(cLORs2, axis=0)
                M1 = np.sum(self.mLORs[m], axis=1)
                M2 = np.sum(self.mLORs[m], axis=0)
                (m1, m2) = self.pairs[m]

                self.C[m1] += C1
                self.C[m2] += C2
                self.M[m1] += M1
                self.M[m2] += M2

            count = 0.0
            for m in range(0, self.N):
                self.sens_c[m] *= self.M[m] / self.C[m]
                count += np.sum(self.sens_c[m])
            factor = count / self.N / self.crystals

            for m in range(0, self.N):
                self.sens_c[m] /= factor

        self.save(iteration)

    def save(self, iteration):
        if self.png != None:
            for i in range(0, self.N):
                scipy.misc.imsave(self.png % (iteration, i), self.sens_c[i].reshape(self.c1, self.c2))

        if self.output != None:
            for i in range(0, self.mpairs):
                self.get_sens(i).tofile('sens_%i.dat' % i)

    def get_sens(self, m):
        (m1, m2) = self.pairs[m]
        return np.outer(self.sens_c[m1], self.sens_c[m2]) * self.sens_m[m]


