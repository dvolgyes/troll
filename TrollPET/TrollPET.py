#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#    TrollPET: Positron Emission Tomography (PET) reconstruction in the Troll project
#    Copyright (C) 2013 David Volgyes
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License version 3 for more details.
#
#    You should have received a copy of the GNU General Public License version 3
#    along with this program (LICENSE.txt).  If not, see http://www.gnu.org/licenses/

#
#    Requirements: nVidia compute capability 2.0+, CUDA 5.0+, PyCuda 2013.1 or newer, Python 2.7+ or 3.2+
#    (It might work with CUDA 4.0+ and Pycuda 2012.1+ too)
#

import sys
import os
import random
import time
import numpy as np
import scipy
import scipy.ndimage.filters
import numpy.fft as fft
from jinja2 import Template
from selfnorm import *

if sys.version_info[0] == 2:

    # Python2 imports

    import ConfigParser
else:

    # Python3 imports

    import configparser as ConfigParser

try:
    import pycuda.driver as drv
    from pycuda.compiler import SourceModule
    import pycuda.tools
    import pycuda.autoinit  # No need for GPU init & GPU shutdown
except:
    pass  # emulation mode, it's incomplete
    sys.exit(1)


def loadArray(
    filename,
    shape,
    default=None,
    dt=np.float32,
):

    if os.path.exists(filename):
        return np.fromfile(filename, dtype=dt).reshape(shape)
    return (np.ones(shape, dtype=dt) * (default if default != None else None))


def readfile(fname):
    with open(fname) as f:
        return f.read()


def get(tupl, index):
    try:
        idx = min(len(tupl) - 1, index)
        return tupl[idx]
    except:
        return tupl


class PETReconstruction(object):

    # Inner variables

    def __init__(self, *args, **kwargs):

        # It's not necessary

        self.voxels = None  # object to store the reconstructed volume
        self.fvoxels = None  # volume which is filtered during the forward projection
        self.isens = None  # isotope sensitivity
        self.cLORs = None  # Calculated LORs
        self.mLORs = None  # Measured LORs
        self.rLORs = None  # background LORs, randoms, etc.
        self.rnd_samples = None
        self.iter = 0
        self.kernelTemplate = ''
        self.cfg = None
        self.sigmas = None
        self.isotopes = None
        self.materials = None
        self.relax = None
        self.posrange = None
        self.subsets = None
        self.LORshape = None
        self.max_samples = None
        self.sample_factor = None
        self.posrange_precalc = None
        self.posrange_point = None
        self.posrange_blurred = None
        self.material_map = None
        self.update_numerator = None
        self.update_denominator = None
        self.timer = time.time()
        self.iter_time = None
        self.senscalc = Sensitivity()
        self.simulation_mode = False

    def configure(self, configfiles):
        configparser = ConfigParser.RawConfigParser()
        configparser.read(configfiles)
        self.cfg = dict()
        for section in configparser.sections():
            for item in configparser.items(section):
                key = '%s_%s' % (section, item[0])
                value = item[1].strip()
                self.cfg['%s_str' % key] = value
                if value.find(',') > -1:
                    self.cfg['%s' % key] = value
                    value = tuple(map(str.strip, value.split(',')))
                    try:
                        value = tuple(map(int, value))
                    except:
                        try:
                            value = tuple(map(float, value))
                        except:
                            pass  # string representation
                else:
                    try:
                        value = int(value)
                    except:
                        try:
                            value = float(value)
                        except:
                            pass  # string representation
                self.cfg['%s' % key] = value

    def setup(self):
        if 'general_workdir' in self.cfg:
            os.chdir(self.cfg['general_workdir'])
        self.projections = self.cfg['reconstruction_projections']
        self.iterations = self.cfg['reconstruction_iteration']

        self.voxelsize = float(self.cfg['volume_voxelsize'])
        self.autonorm_apply = (self.iterations + 10 if self.cfg['sensitivity_apply'] == 0 else self.cfg['sensitivity_apply'])
        self.autonorm_calc = (self.iterations + 10 if self.cfg['sensitivity_calc'] == 0 else self.cfg['sensitivity_calc'])
        self.autonorm_calc = min(self.autonorm_calc, self.autonorm_apply)

        self.materials = self.cfg['reconstruction_materials']
        self.isotopes = self.cfg['reconstruction_isotopes']

        # order isotope, z, y, x:

        volumeshape = (self.isotopes, ) + self.cfg['volume_dimensions'][::-1]
        self.voxels = loadArray(self.cfg['input_initial_volume'], volumeshape, 1)
        self.isens = loadArray(self.cfg['input_isotope_sensitivity'], volumeshape, 1)

        self.fvoxels = self.voxels.copy()

        self.update_numerator = np.zeros_like(self.voxels)
        self.update_denominator = np.ones_like(self.voxels) * 1e-27

        self.posrange_precalc = False

        if self.cfg['reconstruction_positron_range'] > 0:
            self.posrange_point = loadArray(self.cfg['input_positron_range_point'], volumeshape + (self.materials, ), None).astype(np.float64)
            self.posrange_blurred = loadArray(self.cfg['input_positron_range_blurred'], volumeshape + (self.materials, ), None).astype(np.float64)

        self.material_map = loadArray(self.cfg['input_material_map'], volumeshape[0:3], 0, np.uint8)

        self.kernelTemplate = readfile(self.cfg['kernels_template'])
        self.subsets = []
        try:
            for subset in self.cfg['reconstruction_subsets'].split('|'):
                self.subsets.append(list(map(int, subset.strip().split())))
        except:
            self.subsets.append([int(self.cfg['reconstruction_subsets'])])

        random.seed(self.cfg['general_random_seed'])
        if self.cfg['sensitivity_type'] == 'CrystalSensitivity2N':
            self.senscalc = CrystalSensitivity2N()
            self.senscalc.setup(self.cfg['sensitivity_n'], self.cfg['sensitivity_c1'], self.cfg['sensitivity_c2'], self.cfg['sensitivity_coinc'])
        if 'sensitivity_png' in self.cfg:
            self.senscalc.set_png(self.cfg['sensitivity_png'])
        if 'sensitivity_limit' in self.cfg:
            self.senscalc.set_limit(self.cfg['sensitivity_limit'])
        if 'sensitivity_output' in self.cfg:
            self.senscalc.set_output(self.cfg['sensitivity_output'])
        if 'general_simulation_mode' in self.cfg:
            self.simulation_mode = self.cfg['general_simulation_mode'] > 0

    def set_iter(self, it):
        self.iter = it
        self.samples = get(self.cfg['ray_tracing_samples'], it)
        self.rays = get(self.cfg['ray_tracing_rays'], it)
        self.iter_time = time.time()

    def iteration_time(self):
        return time.time() - self.iter_time

    def total_time(self):
        return time.time() - self.timer

    def read_subset(self, NR):
        self.NR = NR  # subset index
        self.LORshape = self.cfg['detector_pair_%s_lor_shape' % NR]
        if self.simulation_mode == True:
            self.cLORs = np.zeros((self.isotopes, ) + self.LORshape, dtype=np.float32)
            return False

        self.mLORs = loadArray(self.cfg['input_measurement'] % NR, (self.isotopes, ) + self.LORshape)
        self.sensitivity = loadArray(self.cfg['input_sensitivity'] % NR, self.mLORs.shape, 1.0)

        if self.autonorm_apply <= self.iter + 1:
            self.sens2 = self.senscalc.get_sens(NR).reshape(self.sensitivity.shape)
        else:
            self.sens2 = np.ones_like(self.sensitivity)

        self.rLORs = loadArray(self.cfg['input_background'] % NR, self.mLORs.shape, 0.0)
        self.cLORs = np.zeros_like(self.mLORs)
        return type(self.mLORs) != type(None)

    def volume_init(self):
        self.fvoxels = self.voxels.copy()

    def gaussian_filter(self):
        if self.cfg['forward_filters_gaussian_filter'] == 0:
            return
        if self.cfg['forward_filters_gaussian_filter'] < self.iter + 1:
            return
        sigmas = 3 * (get(self.cfg['forward_filters_gaussian_sigmas'], self.iter) / self.voxelsize, )
        for iso in range(0, self.isotopes):
            self.fvoxels[iso, :, :, :] = scipy.ndimage.filters.gaussian_filter(self.fvoxels[iso, :, :, :], sigmas, mode='constant')

    def positron_range_model(self):  # TODO should be finished
        if self.cfg['reconstruction_positron_range'] == 0:
            return
        if self.cfg['reconstruction_positron_range'] > self.iter:
            return
        if self.posrange_precalc == False:
            self.posrange_precalc = True
            point = fft.rfftn(self.posrange_point, axes=(0, 1, 2))
            blurred = fft.rfftn(self.posrange_blurred, axes=(0, 1, 2))
            self.posrange = np.nan_to_num(point / blurred)

        for isotope in range(0, self.isotopes):
            result = np.zeros_like(self.posrange[0, 0, :, :, :])
            for material in range(0, self.materials):
                mask = self.material_map[:, :, :] == material
                result += fft.rfftn(self.fvoxels[isotope, :, :, :][mask].reshape(self.fvoxels.shape[2:])) * self.posrange[material, isotope, :, :, :]

#            self.fvoxels[:,:,:,isotope] = np.absolute(fft.irfftn( result, axes = (0,1,2) ))

    # Filtering when a full iteration has ended.

    def volume_filter_after_full_iteration(self):
        if self.cfg['noise_filters_gaussian_filter'] > 0 and self.cfg['noise_filters_gaussian_filter'] <= self.iter + 1:
            sigmas = 3 * (get(self.cfg['noise_filters_gaussian_sigmas'], self.iter) / self.voxelsize, )
            for iso in range(0, self.isotopes):
                self.voxels[iso, :, :, :] = scipy.ndimage.filters.gaussian_filter(self.voxels[iso, :, :, :], sigmas, mode='constant')

        if self.cfg['noise_filters_rank_filter'] > 0 and self.cfg['noise_filters_rank_filter'] <= self.iter + 1:
            size = get(self.cfg['noise_filters_rank_size'], self.iter)
            rank = get(self.cfg['noise_filters_rank'], self.iter)
            factor = get(self.cfg['noise_filters_factor'], self.iter)
            for iso in range(0, self.isotopes):
                tmp = scipy.ndimage.filters.rank_filter(self.voxels[iso, :, :, :], rank=rank, size=size, mode='constant') * factor
                t2 = np.minimum(tmp, self.voxels[iso, :, :, :])
                self.voxels[iso, :, :, :] = t2

    # Filtering when a OSEM subiteration has ended.

    def volume_filter_after_subiteration(self):
        if self.cfg['noise_filters_sub_gaussian_filter'] > 0 and self.cfg['noise_filters_sub_gaussian_filter'] <= self.iter + 1:
            sigmas = 3 * (get(self.cfg['noise_filters_sub_gaussian_sigmas'], self.iter) / self.voxelsize, )

            for iso in range(0, self.isotopes):
                self.voxels[iso, :, :, :] = scipy.ndimage.filters.gaussian_filter(self.voxels[iso, :, :, :], sigmas, mode='constant')

        if self.cfg['noise_filters_sub_rank_filter'] > 0 and self.cfg['noise_filters_sub_rank_filter'] <= self.iter + 1:
            size = get(self.cfg['noise_filters_sub_rank_size'], self.iter)
            rank = get(self.cfg['noise_filters_sub_rank'], self.iter)
            factor = get(self.cfg['noise_filters_sub_factor'], self.iter)

            for iso in range(0, self.isotopes):
                tmp = scipy.ndimage.filters.rank_filter(self.voxels[iso, :, :, :], rank=rank, size=size, mode='constant') * factor
                t2 = np.minimum(tmp, self.voxels[iso, :, :, :])
                self.voxels[iso, :, :, :] = t2

    def volume_update(self):

        self.relax = float(get(self.cfg['reconstruction_relaxation'], self.iter))
        if self.relax != 1.0 and self.relax > 0.0:
            up = np.power(self.update_numerator / self.update_denominator, self.relax)
        else:
            up = self.update_numerator / self.update_denominator
        up = np.clip(np.absolute(np.nan_to_num(up)), 0, 1e10)
        self.voxels *= up
        self.voxels = np.clip(np.nan_to_num(self.voxels), 0, 1e25)
        self.update_numerator.fill(0)
        self.update_denominator.fill(1e-27)

    def random_samples(self):

        # number of random samples can be anything, should be optimized

        self.rnd_samples = np.random.rand(self.cLORs.size).astype(np.float32)

    def forward_project(self):

        self.cLORs.fill(0)
        start = 0
        startOffset = 0

        for i in range(0, self.isotopes):  # Every isotope has contribution to the "main" LORs

            # no need for sensitivity, if we have a single isotope

            fvoxels = (self.fvoxels[i, :, :, :] if type(self.isens) == type(None) or i == 0 else self.fvoxels[i, :, :, :] * self.isens[i, :, :, :])
            cLORs = self.cLORs[0, :, :, :, :]
            end = cLORs.size
            block = (64, 1, 1)
            grid = (1024, int((end - start) / 1024 / 64 + 1))

            self.tpl = Template(self.kernelTemplate.replace('{{NR}}', str(self.NR)))
            self.kernel = self.tpl.render(
                self.cfg,
                RNDTOTAL=self.rnd_samples.size,
                RAYS=self.rays,
                SAMPLES=self.samples,
                THREAD_OFFSET=startOffset,
                TOTAL_THREADS=end - startOffset,
            )

            self.module = SourceModule(self.kernel)

            self.forwardKernel = self.module.get_function('forwardProject')

            self.forwardKernel(drv.InOut(cLORs), drv.In(fvoxels), drv.In(self.rnd_samples), block=block, grid=grid)
            self.cLORs[0, :, :, :] += cLORs

        for i in range(1, self.isotopes):  # Every isotope has contribution to the "main" LORs
            fvoxels = self.fvoxels[i, :, :, :] * (1.0 - self.isens[i, :, :, :])
            cLORs = self.cLORs[i, :, :, :, :]
            end = cLORs.size
            block = (64, 1, 1)
            grid = (1024, (end - start) / 1024 / 64 + 1)

            self.tpl = Template(self.kernelTemplate.replace('{{NR}}', str(self.NR)))
            self.kernel = self.tpl.render(self.cfg, RNDTOTAL=self.rnd_samples.size, SAMPLES=self.samples, THREAD_OFFSET=startOffset, TOTAL_THREADS=end - startOffset)

            self.module = SourceModule(self.kernel)

            self.forwardKernel = self.module.get_function('forwardProject')

            self.forwardKernel(drv.InOut(cLORs), drv.In(fvoxels), drv.In(self.rnd_samples), block=block, grid=grid)
            self.cLORs[i, :, :, :, :] += cLORs

    def save_forward(self, text):
        if 'temp_forward' in self.cfg:
            filename = self.cfg['temp_forward']
            if filename.count('%') == 1:
                filename = filename % self.NR
            if filename.count('%') == 2:
                filename = filename % (self.iter + 1, self.NR)
            print(text % filename)
            self.cLORs.tofile(filename)

    def back_project(self):  # TODO: multi-isotope part should be finished
        self.backKernel = self.module.get_function('backProject')

        update_numerator = np.zeros_like(self.update_numerator[0, :, :, :])
        update_denominator = np.zeros_like(self.update_denominator[0, :, :, :])

        for i in range(0, self.isotopes):
            update_numerator.fill(0)
            update_denominator.fill(0)
            cLORs = self.cLORs[0, :, :, :, :]
            mLORs = self.mLORs[0, :, :, :, :]
            sens = self.sensitivity[0, :, :, :, :] * self.sens2[0, :, :, :, :]

#                isens = self.isens[i,:,:,:]

            start = 0
            end = int(self.cLORs.size)
            block = (64, 1, 1)
            grid = (1024, int((end - start) / 1024 / 64 + 1))

            self.backKernel(
                drv.InOut(update_numerator),
                drv.InOut(update_denominator),
                drv.In(cLORs),
                drv.In(mLORs),
                drv.In(sens),
                drv.In(self.rnd_samples),
                block=block,
                grid=grid,
            )
            drv.Context.synchronize()
            self.update_numerator[i, :, :, :] += update_numerator[:, :, :]
            self.update_denominator[i, :, :, :] += update_denominator[:, :, :]

        return
        for i in range(1, self.isotopes):
            self.update_numerator[i, :, :, :] += update_numerator[:, :, :]
            self.update_denominator[i, :, :, :] += update_denominator[:, :, :]

    def readout_model(self):
        if (self.simulation_mode == False) and (self.cfg['reconstruction_readout_model'] == 0 or self.cfg['reconstruction_readout_model'] > self.iter):
            return
        try:
            sigmas = (0.0, ) + self.cfg['detector_pair_%i_readout_uncertainity' % self.NR]
            self.cLORs = scipy.ndimage.filters.gaussian_filter(self.cLORs, sigmas, mode='constant')
        except:
            print('READOUT MODEL ERROR!')

    def sens_correction(self, text):
        if self.autonorm_calc <= self.iter + 1:
            print(text)
            self.senscalc.add_lors(self.cLORs, self.mLORs, self.rLORs, self.sensitivity, self.NR)

    def noise_model(self):

        # Additive background and random values
        # Values should have variance reduction, e.g. http://dx.doi.org/10.1109/NSSMIC.2008.4774166

        if type(self.rLORs) != type(None):
            self.cLORs += self.rLORs

    def save(self, opt=False):
        name = None
        if type(opt) == str:
            name = opt
        elif opt == True:
            name = self.cfg['output_voxels']
        elif 'output_partial' in self.cfg:
            name = self.cfg['output_partial'] % (self.iter + 1)
        else:
            name = self.cfg['output_voxels']
        self.voxels.tofile(name)
        return name

    def sensitivity_update(self, text):
        if self.autonorm_calc <= self.iter + 1:
            print(text)
            self.senscalc.calculate(self.iter)


# MAIN PROGRAM if the code is not used as a library

if __name__ == '__main__':
    reconstruction = PETReconstruction()
    if len(sys.argv) >= 2:
        cfiles = sys.argv[1:]
    else:
        cfiles = ['reconstruction.cfg']

    reconstruction.configure(cfiles)
    reconstruction.setup()
    print(' ')
    print(' ')
    print('TrollPET reconstruction v.0.2')
    print('   + geometry')  # MC integration, see e.g. http://dx.doi.org/10.1109/TMI.2012.2231693
    print('   + PSF model (gaussian)')  # volumetric blur
    print('   + readout model (gaussian)')  # readout blur in the LOR space
    print('   + volume filters')  # noise filters
    # e.g. László Szirmay-Kalos, Milán Magdics, Balázs Tóth, Tamás Umenhoffer,
    # Judit Lantos, Gergely Patay: Fast Positron Range Calculation in
    # Heterogeneous Media for 3D PET Reconstruction. In: IEEE Nuclear Science
    # Symposium and Medical Imaging Conference, 2012. Anaheim, USA, 2012. pp.
    # 1-2.
    print('   - positron-range correction')
    print('   + self-normalization')  # in progress. A more simple solution: http://www.ncbi.nlm.nih.gov/pubmed/22910096
    print('   - multi-isotope reconstruction')  # http://iopscience.iop.org/0031-9155/56/14/020/

    print(' ')
    print(' Config file(s): %s' % cfiles)
    print(' ')

    if reconstruction.simulation_mode:
        reconstruction.set_iter(0)
        print(' ')
        print(' Simulation mode: forward-only from the initial volume')
        print(' ')
        print('  - volume initialization')
        reconstruction.volume_init()
        print('  - gaussian PSF model')
        reconstruction.gaussian_filter()
        print('  - positron range model')
        reconstruction.positron_range_model()
        print(' ')
        for subset in reconstruction.subsets:
            for detector_pair in subset:
                print('  - setup subset #%s' % detector_pair)
                reconstruction.read_subset(detector_pair)
                print('    - random number generation')
                reconstruction.random_samples()
                print('    - forward project')
                reconstruction.forward_project()
                print('    - readout model')
                reconstruction.readout_model()  # Part of the forward projection, just in a separate call
                reconstruction.save_forward('    - LORs are saved to: %s')
        print('Total running time: %.1f [sec]' % reconstruction.total_time())
    else:
        for i in range(0, reconstruction.iterations):
            reconstruction.set_iter(i)
            print(' ')
            print(' ')
            print('Iteration: #%s,    samples: %s, rays: %s ' % (i + 1, reconstruction.samples, reconstruction.rays))

            if i == 0:  # special case
                print(' ')
                print('  - volume initialization')
                reconstruction.volume_init()
                for subset in reconstruction.subsets:
                    for detector_pair in subset:
                        print('  - load subset #%s' % detector_pair)
                        if reconstruction.read_subset(detector_pair) == False:
                            print('      ! data error')
                            continue
                        print('    - random number generation')
                        reconstruction.random_samples()
                        print('    - forward project')
                        reconstruction.forward_project()
                        reconstruction.save_forward('    - LORs are saved to: %s')
                        reconstruction.sens_correction('    - sensitivity correction')
                        print('    - back project')
                        reconstruction.back_project()
                        print('')
                print('  - volume update')
                reconstruction.volume_update()
            else:
                for subset in reconstruction.subsets:
                    print(' ')
                    print('  - volume initialization')
                    reconstruction.volume_init()
                    print('  - gaussian PSF model')
                    reconstruction.gaussian_filter()
                    print('  - positron range model')
                    reconstruction.positron_range_model()
                    for detector_pair in subset:
                        print('  - load subset #%s' % detector_pair)
                        if reconstruction.read_subset(detector_pair) == False:
                            print('      ! data error')
                            continue
                        print('    - random number generation')
                        reconstruction.random_samples()
                        print('    - forward project')
                        reconstruction.forward_project()
                        print('    - readout model')
                        reconstruction.readout_model()  # Part of the forward projection, just in a separate call
                        reconstruction.save_forward('    - LORs are saved to: %s')
                        reconstruction.sens_correction('    - sensitivity correction')
                        print('    - noise model')  # Not part of the forward projection
                        reconstruction.noise_model()
                        print('    - back project')
                        reconstruction.back_project()
                        print('')
                    print('  - volume update')
                    reconstruction.volume_update()
                    print('  - intra-iteration volume filter')
                    reconstruction.volume_filter_after_subiteration()

            print('  - inter-iteration volume filter')
            reconstruction.volume_filter_after_full_iteration()
            reconstruction.sensitivity_update('  - self-normalization update')
            print('  - the current result is saved to: %s' % reconstruction.save())
            print('  Iteration time: %.1f [sec]' % reconstruction.iteration_time())
        print('')
        print('Final result is saved to: %s' % reconstruction.save(True))
        print('Total running time: %.1f [sec]' % reconstruction.total_time())
