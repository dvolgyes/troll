#!/usr/bin/python

# -*- coding: utf-8 -*-
#
#    TrollPET: Positron Emission Tomography (PET) reconstruction in the Troll project
#    Copyright (C) 2013 David Volgyes
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License version 3 for more details.
#
#    You should have received a copy of the GNU General Public License version 3
#    along with this program (LICENSE.txt).  If not, see http://www.gnu.org/licenses/

#
#    Requirements: lot of memory, Python scikits.image (a.k.a. skimage)
#

import sys,os,random,time,math,re
from skimage.transform.radon_transform import * 
import numpy as np

if sys.version_info[0] == 2:
# Python2 imports
    import ConfigParser
else:
# Python3 imports
    import configparser as ConfigParser

def configure( configfiles ):
        configparser = ConfigParser.RawConfigParser()
        configparser.read(configfiles)
        cfg = dict()
        for section in configparser.sections():
            for item in configparser.items(section):
                key = '%s_%s' % (section, item[0])
                value = item[1].strip()
                cfg['%s_str' % key] = value
                if value.find(',') > -1:
                    cfg['%s' % key] = value
                    value = tuple(map(str.strip, value.split(',')))
                    try:
                        value = tuple(map(int, value))
                    except:
                        try:
                            value = tuple(map(float, value))
                        except:
                            pass  # string representation
                else:
                    try:
                        value = int(value)
                    except:
                        try:
                            value = float(value)
                        except:
                            pass  # string representation
                cfg['%s' % key] = value
        return cfg
        
cfg=configure(sys.argv[1:])
volumefile=cfg['files_volume']
sinogramfile=cfg['files_sinogram']

phi_max = float(cfg['sinogram_phi_max'])
phi = cfg['sinogram_phi']
phi_vector = np.linspace(0., phi_max, phi, endpoint=True)
slices  = cfg['geometry_slices']

iteration = cfg['fbp_iteration'] if 'fbp_iteration' in cfg else 1

sinogram=None
sinogram2=None
volume=None

# File IO
if 'fbp_sensitivity' in cfg:
    print("Sensitivity file: %s" % cfg['fbp_sensitivity'])
    sens=np.fromfile(cfg['fbp_sensitivity'],dtype=np.float32)

if cfg['fbp_reconstruction_str']!='0': # RECON
    print("")
    s="PET FBP reconstruction" if iteration<2 else "PET FBP reprojection & reconstruction"
    print(s)
    print("")
    sinogram=np.fromfile(sinogramfile, dtype=np.float32).reshape( (slices,-1,phi) )
    idx=sens>0
    sinogram[idx] /= sens[idx]
    print("Input:  %s (shape: %s )" % (sinogramfile, sinogram.shape))
    r=sinogram.shape[1]
# projection
else: # projection
    x,y=cfg['volume_x'],cfg['volume_y']
    volume=np.fromfile(volumefile, dtype=np.float32).reshape( (slices,y,x) )
    print("")
    print("PET FBP sinogram generation")
    print("")
    print("Input:  %s (shape: %s )" % (volumefile, volume.shape))
    
if cfg['fbp_reconstruction_str']!='0':
    sens=sens.reshape(sinogram.shape)
    for it in range(1,iteration): # (recon, forward)
        #Recon
        print(" reconstruction ")
        tmp=iradon(sinogram[0,:,:],theta=phi_vector)
        (x,y)=tmp.shape
        if volume==None: volume=np.zeros( (slices,y,x) , dtype=np.float32)
        volume[0,:,:]=tmp    
        for i in range(1,slices):
            if np.any(sinogram[i,:,:]):
                volume[i,:,:]=iradon(sinogram[i,:,:],theta=theta)

        # Reprojection
        print(" reprojection ")
        tmp=radon(volume[0,:,:],theta=phi_vector)
        if sinogram2==None: sinogram2=np.zeros( (slices,tmp.shape[0],tmp.shape[1]) , dtype=np.float32).reshape(sinogram.shape)
        sinogram2[0,:,:]=tmp
        for i in range(1,sinogram.shape[0]):
            if np.any(volume[i,:,:]):
                sinogram2[i,:,:]=radon(volume[i,:,:],theta=phi_vector)
        idx=(sens==0)
        sinogram[idx]=sinogram2[idx]
        if 'files_reprojection' in cfg:
            fn = cfg['files_reprojection']
            if fn.find("%")>=0: fn=fn % it
            print(" reprojection saved to: %s" % fn)
            sinogram.tofile(fn)

if cfg['fbp_reconstruction_str']!='0':
    tmp=iradon(sinogram[0,:,:],theta=phi_vector)
    (x,y)=tmp.shape
    volume=np.zeros( (slices,y,x) , dtype=np.float32)
    volume[0,:,:]=tmp    
    for i in range(1,slices):
        if np.any(sinogram[i,:,:]):
            volume[i,:,:]=iradon(sinogram[i,:,:],theta=theta)
    volume.tofile(volumefile)
    print("Output: %s (shape: z,y,x = %s )" % (volumefile, volume.shape))

else: # projection
    tmp=radon(volume[0,:,:],theta=phi_vector)
    sinogram=np.zeros( (slices,tmp.shape[0],tmp.shape[1]) , dtype=np.float32)
    sinogram[0,:,:]=tmp
    for i in range(1,sinogram.shape[0]):
        if np.any(volume[i,:,:]):
            sinogram[i,:,:]=radon(volume[i,:,:],theta=phi_vector)
    sinogram.tofile(sinogramfile)
    print("Output: %s (shape: z,r,phi = %s )" % (sinogramfile, sinogram.shape))
