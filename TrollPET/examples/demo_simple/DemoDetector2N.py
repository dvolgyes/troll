#!/usr/bin/python

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License version 3 for more details.
#
#    You should have received a copy of the GNU General Public License version 3
#    along with this program (LICENSE.txt).  If not, see http://www.gnu.org/licenses/


import math
import visual
from visual import vector
import numpy as np


def rounder(value):
	if (value>-0.0000000001) and (value<0.0000000001): return 0.0
	return value

def round3(vect):
    v=vector(0,0,0)
    v[0]=rounder(vect[0])
    v[1]=rounder(vect[1])
    v[2]=rounder(vect[2])
    return v

def vec2str(v):
    return "%s, %s, %s" % (v[0],v[1],v[2])

def draw_module(mod,ax,tr,acn,trcn,color="255,0,0"):
    print
#    print mod,ax,tr,acn,trcn, B
    print
    A=ax*acn
    B=tr*trcn
    POINT0=mod+vector(100,100,0)
    POINT1=mod+B+vector(100,100,0)
    return "<line x1='%s' y1='%s' x2='%s' y2='%s' marker-end='url(#triangle)' style='stroke:rgb(%s);stroke-width:2'/>" % (POINT0[0],POINT0[1],POINT1[0],POINT1[1],color)

import sys
import ConfigParser, os
configfiles="demo_detector.cfg"
scannerType="DEMO"

config   = ConfigParser.RawConfigParser()

if scannerType=="DEMO": 
    radius=242
    scannerType="NanoPET"
    modules=8
    axialCN=100
    transCN=50
    axialSize=2
    transSize=2
    axialGapSize=0
    transGapSize=0
    crystalThickness=0
    axialSigma=0.0
    transSigma=0.0

center=vector(0,0,1)
print '<svg xmlns="http://www.w3.org/2000/svg" version="1.1">    <marker id="triangle"      viewBox="0 0 10 10" refX="0" refY="5"       markerUnits="strokeWidth"   markerWidth="4" markerHeight="3" orient="auto"> <path d="M 0 0 L 10 5 L 0 10 z" /> </marker>'

module_geom=[]

for m in range(0,modules):

    angleOffset=math.pi
    angleDirection=1
    angle0=m*angleDirection*(2*math.pi/modules)+angleOffset

    module_origin=vector(-radius,-transCN*(transSize+transGapSize)/2.0 ,-axialCN*(axialSize+axialGapSize)/2.0)
    axVector=vector(0,0,axialSize)
    axGapVector=vector(0,0,axialGapSize)
    transVector=vector(0,transSize,0)
    transGapVector=vector(0,transGapSize,0)

    mod0=round3(visual.rotate(module_origin,angle0,center))
    ax0=round3(visual.rotate(axVector,angle0,center))
    tr0=round3(visual.rotate(transVector,angle0,center))
    ax0gap=round3(visual.rotate(axGapVector,angle0,center))
    tr0gap=round3(visual.rotate(transGapVector,angle0,center))

    module_geom.append( (mod0,ax0,tr0,ax0gap,tr0gap) )

    print draw_module(mod0,ax0,tr0,axialCN,transCN)    

    p0=mod0
    vectax=ax0*axialCN+ax0gap*(axialCN-1)
    vecttr=tr0*transCN+tr0gap*(transCN-1)
    p1=vectax
    p2=vecttr
    p3=p0+vectax+vecttr
    # coordinates for module drawing (gnuplot format: x,y,z,dx,dy,dz)
    p0=round3(p0)
    p1=round3(p1)
    p2=round3(p2)
    p3=round3(p3)

#    print p0[0],p0[1],p0[2],p1[0],p1[1],p1[2]
#    print p0[0],p0[1],p0[2],p2[0],p2[1],p2[2]
#    print p3[0],p3[1],p3[2],-p1[0],-p1[1],-p1[2]
#    print p3[0],p3[1],p3[2],-p2[0],-p2[1],-p2[2]

mp=0

#print module_geom

for m in range(0,modules/2):
    section="detector_pair_%s"%mp
    config.add_section(section)
    mp=mp+1
    m2=(m+modules/2)%modules

#    print m,m2

    mod0=module_geom[m][0]
    ax0=module_geom[m][1]
    tr0=module_geom[m][2]
    ax0gap=module_geom[m][3]
    tr0gap=module_geom[m][4]

    mod1=module_geom[m2][0]
    ax1=module_geom[m2][1]
    tr1=module_geom[m2][2]
    ax1gap=module_geom[m2][3]
    tr1gap=module_geom[m2][4]

#    config.set(section,"module0" , m)
#    config.set(section,"module1" , m2)
    config.set(section,"lor_shape", "%s,%s,%s,%s" % (axialCN,transCN,axialCN,transCN) )
    config.set(section,"readout_uncertainity", "%s,%s,%s,%s" % (axialSigma,transSigma,axialSigma,transSigma) )    

    mod0=round3(mod0)
    mod1=round3(mod1)
    tr0=round3(tr0)
    tr1=round3(tr1)

    config.set(section,"module0_origin" , vec2str(mod0) )
    config.set(section,"module0_axial"  , vec2str(ax0))
    config.set(section,"module0_axial_gap"  , vec2str(ax0gap))    
    config.set(section,"module0_trans"  , vec2str(tr0))
    config.set(section,"module0_trans_gap"  , vec2str(tr0gap))

    config.set(section,"module1_origin" , vec2str(mod1) )
    config.set(section,"module1_axial"  , vec2str(ax1))
    config.set(section,"module1_axial_gap"  , vec2str(ax1gap))    
    config.set(section,"module1_trans"  , vec2str(tr1))
    config.set(section,"module1_trans_gap"  , vec2str(tr1gap))

    if True:
        print draw_module(mod0,ax0,tr0,axialCN,transCN,"0,255,0")
        print draw_module(mod1,ax1,tr1,axialCN,transCN,"0,255,0")

for coinc in range(1,modules/2):
    for m in range(0,modules):

        section="detector_pair_%s"%mp
        config.add_section(section)
        mp=mp+1
        m2=(m+modules/2+coinc)%modules

#        print "MODULES %s %s" % (m,m2)

        config.set(section,"lor_shape", "%s,%s,%s,%s" % (axialCN,transCN,axialCN,transCN) )
        config.set(section,"readout_uncertainity", "%s,%s,%s,%s" % (axialSigma,transSigma,axialSigma,transSigma) )    


        mod0=module_geom[m][0]
        ax0=module_geom[m][1]
        tr0=module_geom[m][2]
        ax0gap=module_geom[m][3]
        tr0gap=module_geom[m][4]

        mod1=module_geom[m2][0]
        ax1=module_geom[m2][1]
        tr1=module_geom[m2][2]
        ax1gap=module_geom[m2][3]
        tr1gap=module_geom[m2][4]

        config.set(section,"module0" , m)
        config.set(section,"module1" , m2)

        config.set(section,"module0_origin" , vec2str(mod0) )
        config.set(section,"module0_axial"  , vec2str(ax0))
        config.set(section,"module0_axial_gap"  , vec2str(ax0gap))    
        config.set(section,"module0_trans"  , vec2str(tr0))
        config.set(section,"module0_trans_gap"  , vec2str(tr0gap))

        config.set(section,"module1_origin" , vec2str(mod1) )
        config.set(section,"module1_axial"  , vec2str(ax1))
        config.set(section,"module1_axial_gap"  , vec2str(ax1gap))    
        config.set(section,"module1_trans"  , vec2str(tr1))
        config.set(section,"module1_trans_gap"  , vec2str(tr1gap))

        print draw_module(mod0,ax0,tr0,axialCN,transCN,"0,0,255")
        print draw_module(mod1,ax1,tr1,axialCN,transCN,"0,0,255")

config.write(open(configfiles,"w+t"))
print '</svg>'
