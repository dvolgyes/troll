#!/usr/bin/python
import sys
import numpy as np

def L_p(volume1,volume2,p):
    return np.power(np.sum(np.power(np.abs(volume1-volume2),p)/volume1.size),1.0/p)

def L_0(volume1,volume2):
    return np.sum(volume1==volume2)

def L_inf(volume1,volume2):
    return np.max(np.abs(volume1-volume2))

for name in sys.argv[2:]:
    volume1=np.fromfile(sys.argv[1],dtype=np.float32)
    volume2=np.fromfile(name,dtype=np.float32)
    volume1/=volume1.size
    volume2/=volume2.size


    L1=L_p(volume1,volume2,1)
    L2=L_p(volume1,volume2,2)
    Lp=L_p(volume1,volume2,1.5)

    volume1/=np.sum(volume1)
    volume2/=np.sum(volume2)

    L1scaled=L_p(volume1,volume2,1)
    L2scaled=L_p(volume1,volume2,2)
    Lpscaled=L_p(volume1,volume2,1.5)
    print("%s %s %s %s %s %s" % (L1,L2,Lp,L1scaled,L2scaled,Lpscaled))


