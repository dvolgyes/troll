Troll: Tomographic Reconstruction Of Loony Lines
================================================


Introduction
------------

This project does not want to be a feature complete, commercial grade product. It's a one man project where the goal is to publish proof of concept codes and share ideas. There are similar open source reconstruction projects, like [STIR](http://stir.sf.net), or [OSCaR](http://www.cs.toronto.edu/~nrezvani/OSCaR.html) for reconstruction, and several for simulations, e.g. [GATE](http://www.opengatecollaboration.org/), [CTSim](http://www.ctsim.org).

Unlike them, Troll does not want to be a "serious" project or a "mature" project. I work on it when I have spare time. 

Inspiration
-----------

I worked on several reconstruction projects. Some were better, some were worse, but the initial learning cost was always high. STIR is a very well designed and maintained project, but the C++ language, the requirement of portability and the generality (file formats, scanners, etc.) brought complexity and complexity means a lot of source lines and hard learning curve. I decided to design and write a PET reconstruction in PyCUDA. It was ugly, but it worked. Next step is to redesign the code inspired by [code kata](http://codekata.pragprog.com/).

PET, CT or SPECT
----------------

Well, the difference between [PET](https://en.wikipedia.org/wiki/Positron_emission_tomography), [CT](https://en.wikipedia.org/wiki/X-ray_computed_tomography) or [SPECT](https://en.wikipedia.org/wiki/Single-photon_emission_computed_tomography) is quite small, if you look them from the mathematical point of view.  The main task is an inverse [radion transformation](https://en.wikipedia.org/wiki/Radon_transform) which is quite well known. However, there are some tricks like:

  *  modeling physical effects
  *  speed, efficiency
  *  signal-to-noise ratio

If you have a sinogram from any tomography, you can do an inverse radon transformation, and it will make some sense. However, handling the these issues is quite modality specific. So right now you will find an efficient PET implementation, and later I will add CT and SPECT support too. Any test data contribution is welcomed.


License and contribution
------------------------

From legal point of view the license is GPL v3, you can read the details in the LICENSE.txt. However, there is an unofficial license requirement: if you use the code or data from the project for scientific publication, then you should properly cite the components. The proper way depends on the situation. If you use simulation data from a contributor, then cite the contributors article. If you use or modify the reconstruction code, then cite the related articles. If you need a lot of help and colaboration or if you publish about an idea which appeared in the project first, then co-authorship would be nice. 

To help the proper citation check the "references.txt" in the directories and for the human readable files (e.g. txt, python codes,etc.) check the headers.

Any contribution is welcome, like sending patches or editing the wiki. However, if you send file(s), then you have to agree with the GPL v3 license, and you have to own the rights to publish the files. E.g. if you work on a secret project for a company, and you send measurement data to the project, then you will be responsible for the data breach. 

Support
-------

No official support and no promises. I definitely will not write your Ph.D. thesis, or large part of your future article. You have to do this, or you shouldn't be a scientist. I help when I have time and I am in the mood. But I cannot and I do not want to handle requests like "I want to simulate and reconstruct a new PET scanner, but I have no idea about anything, please, help!".

However, there is an [e-mail list](https://groups.google.com/d/forum/troll-recon) at Google Groups, feel free to join.

Credits
-------

Of course, everything has a past. This project is based on the [COMPET project](http://www.mn.uio.no/fysikk/forskning/prosjekter/compet/) which was a research project at the University of Oslo. The aim was to develop a high sensitivity, high resolution MRI compatible preclinical PET scanner. It's mostly a hardware project, but hardware needs software, so I wrote a reconstruction code for the project. The current project ("Troll") uses the experience and results of the COMPET project but it's an independent project.
